import { Component, OnInit } from '@angular/core';
import { GoogleChartService } from '../service/google-chart.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  private glib: any;

  constructor(private gChartService: GoogleChartService) {
    this.glib = this.gChartService.getGoogle();
    this.glib.charts.load('current',{'packages':['corechart', 'table']});
    this.glib.charts.setOnLoadCallback(this.drawChart.bind(this));
   }

  ngOnInit(): void {
  }

  private drawChart(){
    let chart = new this.glib.visualization.PieChart(document.getElementById('divPieChart'));
    let data = new this.glib.visualization.DataTable();
    data.addColumn('string', 'Accessories');
    data.addColumn('number', 'Quantity');
    data.addRows([
      ['Computers', 3],
      ['Hard Drives', 6],
      ['Printers', 4],
      ['Monitors', 5],
      ['RAM', 2],
    ]);

    let options = {'title':'Sales Info'};

    chart.draw(data, options);
  }

}
